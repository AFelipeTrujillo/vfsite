/* Theme Name: Techtiq - Responsive One Page Template
   Author: Themesdesign
   Version: 1.0.0
   Created: May 2017
   File Description: Main JS file of the template
*/



/*global jQuery */
jQuery(function ($) {
    'use strict';

    /**
        Main application module
    */
    var App = {
        $options: {},
        $htmlBody: $("html, body"),
        $backToTop: $(".back-to-top"),
        $nav: $("nav"),
        $counterProject: $("#counter-pro"),
        $counterClient: $('#counter-client'),
        $counterWork: $('#counter-works'),
        $counterAward: $('#counter-award'),
        $loader: $(".loader"),
        $animationload: $(".animationload"),
        $navbarLink: $('.navbar-nav a'),
        $testiSlider: $("#testi-carousel"),
        $homeSlider: $("#main-home-carousel"),
        $imagesToShow: $(".show-image"),
        $typeTextElement: $(".element"),

        /**
         * init mangific popup
         */
        initMagnificPopoup: function() {
            this.$imagesToShow.magnificPopup({
                type: 'image'
            });
        },

        /**
         * Init easy pie
         * @param el
         */
        initEasyPieChart: function(el) {
            el.circliful();
        },

        /**
         * Init typed text
         */
        initTypeText: function() {
            this.$typeTextElement.each(function(){
                var $this = $(this);
                $this.typed({
                    strings: $this.attr('data-elements').split(','),
                    typeSpeed: 100, // typing speed
                    backDelay: 3000 // pause before backspacing
                });
            });
        },

        /**
         * Binding events
         */
        bindEvents: function () {
            //binding events
            $(window).on('scroll', this.scroll.bind(this));
            $(document).on('ready', this.docReady.bind(this));
        },
        //window scroll event
        scroll: function (event) {
            if ($(window).scrollTop() > 100) {
                this.$backToTop.fadeIn();
            } else {
                this.$backToTop.fadeOut();
            }

            if ($(window).scrollTop() > 80) {
                this.$nav.addClass('small');
            } else {
                this.$nav.removeClass('small');
            }
        },
        //document ready event
        docReady: function () {

            //this
            var $this = this;

            this.$counterProject.counterUp({
                delay: 50,
                time: 10000
            });

            this.$counterClient.counterUp({
                delay: 50,
                time: 5000
            });

            this.$counterWork.counterUp({
                delay: 50,
                time: 7000
            });

            this.$counterAward.counterUp({
                delay: 50,
                time: 12000
            });


            this.$loader.delay(300).fadeOut();
            this.$animationload.delay(600).fadeOut("slow");

            this.$backToTop.on("click", function(){
                $this.$htmlBody.animate({ scrollTop: 0 }, 1000);
                return false;
            });

            this.$navbarLink.on("click", function(event) {
                var $anchor = $(this);
                $this.$htmlBody.stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top - 50
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            });

            $(window).stellar({
                horizontalScrolling: false,
                responsive: true,
                scrollProperty: 'scroll',
                parallaxElements: false,
                horizontalOffset: 0,
                verticalOffset: 0
            });

            if(this.$testiSlider.length > 0) {
                this.$testiSlider.owlCarousel({
                    // Most important owl features
                    items: 1,
                    itemsCustom: false,
                    itemsDesktop: [1199, 1],
                    itemsDesktopSmall: [980, 1],
                    itemsTablet: [768, 1],
                    itemsTabletSmall: false,
                    itemsMobile: [479, 1],
                    singleItem: false,
                    startDragging: true,
                    autoPlay: true
                });
            }

            if(this.$homeSlider.length > 0) {
                this.$homeSlider.owlCarousel({
                    // Most important owl features
                    paginationSpeed: 1000,
                    pagination: false,
                    navigation: false,
                    singleItem: true,
                    slideSpeed: 600,
                    autoPlay: 5000
                });
            }

            //magnify popup
            this.initMagnificPopoup();
            this.initTypeText();
            //initialize the easy pie
            this.initEasyPieChart($('#mySkill1'));
            this.initEasyPieChart($('#mySkill2'));
            this.initEasyPieChart($('#mySkill3'));
            this.initEasyPieChart($('#mySkill4'));
        },
        init: function (_options) {
            $.extend(this.$options, _options);
            this.bindEvents();
        }
    };

    //Initializing the app
    App.init({});

});


/* ==============================================
Contact
=============================================== */
(function($) {
    "use strict";
    jQuery(document).ready(function() {
        $('#cform').submit(function() {

            var action = $(this).attr('action');

            $("#message").slideUp(750, function() {
                $('#message').hide();

                $('#submit')
                    .before('<img src="img/ajax-loader.gif" class="contact-loader" />')
                    .attr('disabled', 'disabled');

                $.post(action, {
                        name: $('#name').val(),
                        email: $('#email').val(),
                        comments: $('#comments').val(),
                    },
                    function(data) {
                        document.getElementById('message').innerHTML = data;
                        $('#message').slideDown('slow');
                        $('#cform img.contact-loader').fadeOut('slow', function() {
                            $(this).remove()
                        });
                        $('#submit').removeAttr('disabled');
                        if (data.match('success') != null) $('#cform').slideUp('slow');
                    }
                );

            });

            return false;

        });

    });
}(jQuery));